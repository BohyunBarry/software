/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package software;

import java.util.Scanner;

/**
 *
 * @author D00162078
 */
public class Software {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Enter the decimal number:");
        int n = kb.nextInt();
        System.out.println(IntegerToRoman(n));
//        System.out.println("Enter the roman number:");
//        int r = kb.nextInt();
//        System.out.println(RomanToInteger());
    }

    public static String IntegerToRoman(int n) {
        String roman = "";
        int repeat;
        int magnitude[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String symbol[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        for (int x = 0; n > 0; x++) {
            repeat = n / magnitude[x];
            for (int i = 1; i <= repeat; i++) {
                roman = roman + symbol[x];
            }
            n = n % magnitude[x];
        }
        return roman;
    }

    public static int[] RomanToInteger(String symbol[]) {
        int[] integer = new int[symbol.length];
        for (int i = 0; i < symbol.length; i++) {
            String op = symbol[i];
            switch (op) {
                case "I":
                    integer[i] = 1;
                    break;
                case "V":
                    integer[i] = 5;
                    break;
                case "X":
                    integer[i] = 10;
                    break;
                case "L":
                    integer[i] = 50;
                    break;
                case "C":
                    integer[i] = 100;
                    break;
                case "D":
                    integer[i] = 500;
                    break;
                case "M":
                    integer[i] = 1000;
                    break;
            }
        }
return integer;
    }

}
